--SQL Day 02

--cast
select cast(10 as decimal(18,4))
select cast('10' as int)
select cast(10.65 as int)
select cast('2023-03-16' as datetime)
select GETDATE(),GETUTCDATE()
select day(getdate()), month(getdate()), year(getdate())

--convert
select convert(decimal(18,4), 10)
select convert(int, '10')
select convert(int, 10.65)
select convert(datetime, '2023-03-16')

--dateadd
select DATEADD(year,2,getdate()), DATEADD(month,3,getdate()), DATEADD(day, 5, getdate())

--datediff
select DATEDIFF(day,'2023-03-16','2023-03-25'),datediff(month,'2023-03-16','2024-06-16')
select DATEDIFF(year,'2023-03-16','2030-03-16')

--sub query
select name,address,email,panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa)

insert into [dbo].[mahasiswa]
select name,address,email,panjang From mahasiswa

--create view
create view vwMahasiswa2
as
select * From mahasiswa

--select view
select * from  vwMahasiswa2

--delete view
drop view vwMahasiswa2

--create index
create index index_name
on mahasiswa(name)

create index index_address_email
on mahasiswa(address,email)

select * from mahasiswa where name='toni'

--create unique index
create unique index uniqueindex_address
on mahasiswa(address)

--drop index
drop index index_address_email on mahasiswa

--drop unique index
drop index uniqueindex_address on mahasiswa

select * from mahasiswa
select * from biodata

--add primary key
alter table mahasiswa
add constraint pk_id_address primary key(id,address)

--drop primary key
alter table mahasiswa drop constraint pk_address

--add unique constraint
alter table mahasiswa 
add constraint unique_address unique(address)

alter table mahasiswa 
add constraint unique_panjang unique(panjang)

update mahasiswa set panjang=null where id=5

--drop unique
alter table mahasiswa drop constraint unique_address

