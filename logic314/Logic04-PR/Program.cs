﻿using System;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //SoalPR_1();
            //SoalPR_2();
            //SoalPR_3();
            //SoalPR_4();
            //SoalPR_5();
            //SoalPR_6();
            //SoalPR_7();
            //SoalPR_8();
            //SoalPR_9();
            //SoalPR_10();
            //SoalPR_11();
            //SoalPR_12();

            Console.ReadKey();
        }

        static void SoalPR_12()
        {
            Console.WriteLine("--Array Staircase 2 Dimensi--");
            Console.Write("Masukkan input N : ");
            int input = int.Parse(Console.ReadLine());

            for (int i = 1; i <= input; i++)
            {
                for (int j = 1; j <= input; j++)
                {
                    if (i == 1 || i == input)
                    {
                        if (i == input)
                        {
                            Console.Write($"{input - j + 1}\t");
                        }
                        else
                        {
                            Console.Write($"{j}\t");
                        }
                    }
                    else
                    {
                        if (j == 1 || j == input)
                        {
                            Console.Write("*\t");
                        }
                        else
                        {
                            Console.Write(" \t");
                        }
                    }
                }
                Console.WriteLine();
            }
        }

        static void SoalPR_11()
        {
            Console.WriteLine("--Belanja Lebaran--");
            Console.Write("Masukkan Uang Andi : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Harga Baju (pakai koma) : ");
            int[] hargaBajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan Harga Celana (pakai koma) : ");
            int[] hargaCelanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
            for(int i = 0; i < hargaBajuArray.Length; i++)
            {
                for(int j = 0; j < hargaCelanaArray.Length; j++)
                {
                    int harga = hargaBajuArray[i] + hargaCelanaArray[j];
                    if (harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }
                }
            }

            Console.WriteLine($"Maksimal Uang yang dibelanjakan Andi : {maxBelanja}");
        }

        static void SoalPR_10()
        {
            Console.WriteLine("--IMP Fashion (Baju)--");
            Console.Write("Masukkan Kode Baju : ");
            int kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Ukuran : ");
            string kodeUkuran = Console.ReadLine().ToUpper();

            if (kodeBaju == 1)
            {
                Console.WriteLine("Merk Baju = IMP");
                if(kodeUkuran == "S")
                {
                    Console.WriteLine("Harga = 200000");
                }
                else if (kodeUkuran == "M")
                {
                    Console.WriteLine("Harga = 220000");
                }
                else
                {
                    Console.WriteLine("Harga = 250000");
                }
            }
            else if (kodeBaju == 2)
            {
                Console.WriteLine("Merk Baju = Prada");
                if (kodeUkuran == "S")
                {
                    Console.WriteLine("Harga = 150000");
                }
                else if (kodeUkuran == "M")
                {
                    Console.WriteLine("Harga = 160000");
                }
                else
                {
                    Console.WriteLine("Harga = 170000");
                }
            }
            else if (kodeBaju == 3)
            {
                Console.WriteLine("Merk Baju = Gucci");
                if (kodeUkuran == "S")
                {
                    Console.WriteLine("Harga = 200000");
                }
                else if (kodeUkuran == "M")
                {
                    Console.WriteLine("Harga = 200000");
                }
                else
                {
                    Console.WriteLine("Harga = 200000");
                }
            }
        }

        static void SoalPR_9()
        {
            Console.WriteLine("--Time Conversion--");
            Console.Write("Masukkan input Time (PM/AM) : ");
            string input = Console.ReadLine();

            string PM = input.Substring(8, 2);
            
            string hasil = "";

            if (PM == "PM")
            {
                int jam = int.Parse(input.Substring(0, 2));
                jam += 12;
                jam = jam >= 24 ? jam - 24 : jam;
                hasil = jam.ToString().PadLeft(2,'0') + input.Substring(2, 6);
            }
            else
            {
                hasil = input.Replace("AM", "").Replace("PM", "");
            }
            Console.WriteLine(hasil);
        }

        static void SoalPR_8()
        {
            Console.WriteLine("--Fibonaci 2--");
            Console.Write("Masukkan input N : ");
            int input = int.Parse(Console.ReadLine());

            int[] temp = new int[input];
            for (int i = 0; i < input; i++)
            {
                if (i <= 1)
                {
                    temp[i] = 1;
                }
                else
                {
                    temp[i] = temp[i - 1] + temp[i - 2];
                }
            }
            Console.WriteLine(String.Join(",", temp));
        }

        static void SoalPR_7()
        {
            Console.WriteLine("-- -5 10 -15 20 -25 30 -35 --");
            Console.Write("Masukkan input N : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kelipatan : ");
            int kelipatan = int.Parse(Console.ReadLine());

            int hasil = 0, temp = 0;
            for (int i = 0; i < input; i++)
            {
                //if (i == 0)
                //{
                //    hasil = kelipatan;
                //    Console.Write($"{hasil}\t");
                //}
                //else
                //{
                    hasil += kelipatan;
                    if (i % 2 == 0)
                    {
                        Console.Write($"{hasil}\t");
                    }
                    else
                    {
                        Console.Write($"{hasil * (-1)}\t");
                    }
                //}
                //temp = hasil;
            }
        }

        static void SoalPR_6()
        {
            Console.WriteLine("-- 3 * 27 * 243 * 2187 --");
            Console.Write("Masukkan input N : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Perkalian : ");
            int perkalian = int.Parse(Console.ReadLine());

            int hasil = 0, temp = 0;
            for (int i = 0; i < input; i++)
            {
                if(i == 0)
                {
                    hasil = perkalian;
                    Console.Write($"{hasil}\t");
                }
                else
                {
                    hasil *= perkalian;
                    if (i % 2 == 0)
                    {
                        Console.Write($"{hasil}\t");
                    }
                    else
                    {
                        Console.Write($"*\t");
                    }
                }
                temp = hasil;
            }
        }

        static void SoalPR_5()
        {
            Console.WriteLine("--saya pasti bisa--");
            Console.Write("Masukkan kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");

            string hasil = "";
            for (int i = 0; i < kalimat.Length; i++)
            {
                char[] array = kalimat[i].ToCharArray();
                for (int j = 0; j < array.Length; j++)
                {
                    if (j != 0)
                    {
                        hasil += array[j];
                        if(j == array.Length - 1)
                        {
                            hasil += " ";
                        }
                    }
                }
            }
            Console.WriteLine($"{hasil}");
        }

        static void SoalPR_4()
        {
            Console.WriteLine("--Aku Mau Makan--");
            Console.Write("Masukkan kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");

            string hasil = "";
            for (int i = 0; i < kalimat.Length; i++)
            {
                char[] array = kalimat[i].ToCharArray();
                for (int j = 0; j < array.Length; j++)
                {
                    if (j == 0 || j == array.Length - 1)
                    {
                        hasil += "*";
                        if (j == array.Length - 1)
                        {
                            hasil += " ";
                        }
                    }
                    else
                    {
                        hasil += array[j];
                    }
                }
            }
            Console.WriteLine($"{hasil}");
        }

        static void SoalPR_3()
        {
            Console.WriteLine("--A*u S****g K**u--");
            Console.Write("Masukkan kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");

            string hasil = "";
            for (int i = 0; i < kalimat.Length; i++)
            {
                char[] array = kalimat[i].ToCharArray();
                for(int j = 0; j < array.Length; j++)
                {
                    if (j == 0 || j == array.Length - 1)
                    {
                        hasil += array[j];
                        if (j == array.Length - 1)
                        {
                            hasil += " ";
                        }
                    }
                    else
                    {
                        hasil += "*";
                    }
                }
            }
            Console.WriteLine($"{hasil}");
        }

        static void SoalPR_2()
        {
            Console.WriteLine("--Total Kata--");
            Console.Write("Masukkan kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");

            for(int i = 0; i < kalimat.Length; i++)
            {
                Console.WriteLine($"Kata {i+1} = {kalimat[i]}");
            }
            Console.WriteLine($"Total kata adalah {kalimat.Length}");
        }

        static void SoalPR_1()
        {
            Console.WriteLine("--Gaji Golongan Karyawan--");
            Console.Write("Masukkan golongan : ");
            int golongan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jam kerja : ");
            int jamKerja = int.Parse(Console.ReadLine());

            double upah = 0, lembur = 0, total = 0;
            int upahGol1 = 2000, upahGol2 = 3000, upahGol3 = 4000, upahGol4 = 5000;

            if (golongan == 1)
            {
                if(jamKerja > 40)
                {
                    upah = upahGol1 * 40;
                    lembur = upahGol1 * (jamKerja - 40) * 1.5;
                }
                else
                {
                    upah = upahGol1 * jamKerja;
                }
            }
            else if (golongan == 2)
            {
                if (jamKerja > 40)
                {
                    upah = upahGol2 * 40;
                    lembur = upahGol2 * (jamKerja - 40) * 1.5;
                }
                else
                {
                    upah = upahGol2 * jamKerja;
                }
            }
            else if (golongan == 3)
            {
                if (jamKerja > 40)
                {
                    upah = upahGol3 * 40;
                    lembur = upahGol3 * (jamKerja - 40) * 1.5;
                }
                else
                {
                    upah = upahGol3 * jamKerja;
                }
            }
            else if (golongan == 4)
            {
                if (jamKerja > 40)
                {
                    upah = upahGol4 * 40;
                    lembur = upahGol4 * (jamKerja - 40) * 1.5;
                }
                else
                {
                    upah = upahGol4 * jamKerja;
                }
            }
            total = upah + lembur;

            Console.WriteLine($"Upah : {upah}");
            Console.WriteLine($"Lembur : {lembur}");
            Console.WriteLine($"Total : {total}");
        }

    }
}
