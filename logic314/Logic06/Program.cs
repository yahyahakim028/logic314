﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //contohClass();
            //list();
            //listClass();
            //listAdd();
            //listRemove();

            Console.ReadKey();
        }

        static void listRemove()
        {
            Console.WriteLine("--List Remove--");
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            listUser.Add(new User() { Name = "Anwar", Age = 24 });
            listUser.RemoveAt(2);

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + " tahun");
            }
        }

        static void listAdd()
        {
            Console.WriteLine("--List Add--");
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            listUser.Add(new User() { Name = "Anwar", Age = 24 });

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + " tahun");
            }
        }

        static void listClass()
        {
            Console.WriteLine("--List--");
            List<User> listUser = new List<User>()
            {
                new User(){Name = "Isni Dwitiniardi", Age = 22},
                new User(){Name = "Astika", Age = 23},
                new User(){Name = "Alfi Azizi", Age = 23}
            };

            for (int i = 0; i < listUser.Count; i++){
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + " tahun");
            }
        }

        static void contohClass()
        {
            Mobil mobil = new Mobil("RI SATU");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatNo();

            Console.WriteLine($"Plat Nomor : {platno}");
            Console.WriteLine($"Bensin : {mobil.bensin}");
            Console.WriteLine($"Kecepatan : {mobil.kecepatan}");
            Console.WriteLine($"Posisi : {mobil.posisi}");
        }

        static void list()
        {
            Console.WriteLine("--List--");

            List<string> list = new List<string>()
            {
                "Astika",
                "Marchelino",
                "Alwi Fadli",
                "Toni"
            };

            Console.WriteLine(String.Join(", ", list));
        }
    }
}
