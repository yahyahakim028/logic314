﻿using System;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Tugas1();
            //Tugas2();
            //Tugas3();
            //Tugas4();
            //perulanganWhile();
            //perulanganDoWhile();
            //perulanganForIncreement();
            //perulanganForDecreement();
            //Break();
            //Continue();
            //forBersarang();
            //arrayStatic();
            //arrayForeach();
            //arrayFor();
            //array2Dimensi();
            //array2DimensiFor();
            //splitJoin();
            //subString();
            //stringToCharArray();

            Console.ReadKey();
        }

        static void stringToCharArray()
        {
            Console.WriteLine("--String ToCharArray--");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();

            foreach (char ch in array)
            {
                Console.WriteLine(ch);
            }
        }

        static void subString()
        {
            Console.WriteLine("--Sub String--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring(1,4) : " + kalimat.Substring(1, 4));
            Console.WriteLine("Substring(5,2) : " + kalimat.Substring(5, 2));
            Console.WriteLine("Substring(7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring(9) : " + kalimat.Substring(9));
        }

        static void splitJoin()
        {
            Console.WriteLine("--Split dan Join--");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split("a");

            foreach(string kata in kataArray)
            {
                Console.WriteLine(kata);
            }

            Console.WriteLine(string.Join(",", kataArray));
        }

        static void array2DimensiFor()
        {
            int[,] array = new int[,]
            {
                { 1,2,3 },
                { 4,5,6 },
                { 7,8,9 }
            };

            //cetak menggunakan for
            for (int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    Console.Write(array[i,j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void array2Dimensi()
        {
            int[,] array = new int[,] 
            {
                { 1,2,3 },
                { 4,5,6 },
                { 7,8,9 }
            };

            //cetak
            Console.WriteLine(array[0, 1]);//2
            Console.WriteLine(array[1, 2]);//6
            Console.WriteLine(array[2, 1]);//8
            Console.WriteLine(array[2, 2]);//9
        }

        static void arrayFor()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi Fadli Siregar"
            };

            //baca array menggunakan for
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void arrayForeach()
        {
            //3 Cara deklarasi array
            string[] a = { "a","b","c" };
            string[] b = new string[3];
            b[0] = "a";
            b[1] = "b";
            b[2] = "c";
            string[] c = new string[]
            {
                "a",
                "b",
                "c"
            };

            string[] array = new string[] 
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi Fadli Siregar"
            };

            //baca array menggunakan foreach
            foreach (string item in array)
            {
                Console.WriteLine(item);
            }
        }

        static void arrayStatic()
        {
            Console.WriteLine("--Array--");
            int[] staticIntArray = new int[3];

            //isi array
            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;

            //cetak array
            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);
        }

        static void forBersarang()
        {
            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                //Console.WriteLine();
                Console.Write("\n");
            }
        }

        static void Continue()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void Break()
        {
            for(int i = 0; i < 10; i++)
            {
                if(i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        static void perulanganForDecreement()
        {
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }

        static void perulanganForIncreement()
        {
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void perulanganDoWhile()
        {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            } while (a < 5);
        }

        static void perulanganWhile()
        {
            Console.WriteLine("--Perulangan While--");
            bool ulangi = true;
            int nilai = 1;

            while (ulangi)
            {
                Console.WriteLine($"proses ke : {nilai}");
                nilai++;

                Console.Write($"Apakah anda akan mengulangi proses ? (y/n)");
                string input = Console.ReadLine().ToLower();

                if(input == "y")
                {
                    ulangi = true;
                }
                else if(input == "n")
                {
                    ulangi = false;
                }
                else
                {
                    nilai = 1;
                    Console.WriteLine("Input yang anda masukkan salah");
                    ulangi = true;
                }
            }
        }

        static void Tugas4()
        {
            Console.WriteLine("--Ganjil Genap--");
            Console.Write("Masukkan nilai : ");
            int input = int.Parse(Console.ReadLine());

            if (input % 2 == 0)
            {
                Console.WriteLine($"Angka {input} adalah genap");
            }
            else
            {
                Console.WriteLine($"Angka {input} adalah ganjil");
            }
        }

        static void Tugas3()
        {
            Console.WriteLine("--Grade Nilai--");
            Console.Write("Masukkan nilai : ");
            int input = int.Parse(Console.ReadLine());

            if (input >= 80 && input <= 100)
            {
                Console.WriteLine("Anda mendapatkan nilai Grade A");
            }
            else if (input >= 60 && input < 80)
            {
                Console.WriteLine("Anda mendapatkan nilai Grade B");
            }
            else if (input >= 0 && input < 60)
            {
                Console.WriteLine("Anda mendapatkan nilai Grade C");
            }
            else
            {
                Console.WriteLine("Anda salah memasukkan nilai");
            }
        }

        static void Tugas2()
        {
            Console.WriteLine("--Puntung Rokok--");
            Console.Write("Masukkan jumlah puntung rokok : ");
            int input = int.Parse(Console.ReadLine());

            int hasilRangkaiRokok = input / 8;
            int sisapuntung = input % 8;

            Console.WriteLine($"Jumlah rokok yang berhasil dirangkai : {hasilRangkaiRokok}");
            Console.WriteLine($"Sisa puntung rokok : {sisapuntung}");
            Console.WriteLine($"Total Penghasilan : Rp. {hasilRangkaiRokok * 500}");
        }

        static void Tugas1()
        {
            Console.WriteLine("--Modulus--");
            Console.Write("Masukkan nilai angka : ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai pembagi : ");
            int pembagi = int.Parse(Console.ReadLine());

            if(angka % pembagi == 0)
            {
                Console.WriteLine($"Angka (variable angka) % (variable pembagi) adalah {angka % pembagi}");
            }
            else
            {
                Console.WriteLine($"Angka (variable angka) % (variable pembagi) bukan 0 melainkan hasil {angka % pembagi}");
            }
        }

    }
}
