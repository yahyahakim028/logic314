﻿using System;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ifStatement();
            //elseStatement();
            //ifNested();
            //ternary();
            //switchCase();
            //Tugas1();
            //Tugas2();

            Console.ReadKey();
        }

        static void Tugas1()
        {
            Console.WriteLine("--Mencari Luas dan Keliling Lingkaran--");
            Console.Write("Masukkan panjang jari-jari : ");
            double input = double.Parse(Console.ReadLine());

            const double phi = 3.14;

            double luas = phi * input * input;
            double keliling = 2 * phi * input;

            Console.WriteLine($"Luas Lingkaran : {luas}");
            Console.WriteLine($"Keliling Lingkaran : {keliling}");
        }

        static void Tugas2()
        {
            Console.WriteLine("--Mencari Luas dan Keliling Persegi--");
            Console.Write("Masukkan panjang sisi : ");
            double input = double.Parse(Console.ReadLine());

            double luas = input * input;
            double keliling = 4 * input;

            Console.WriteLine($"Luas Persegi : {luas}");
            Console.WriteLine($"Keliling Persegi : {keliling}");
        }

        static void switchCase()
        {
            Console.WriteLine("--Switch Case--");
            Console.WriteLine("Pilih buah kesukaan kalian (apel,mangga,pisang)");
            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "apel":
                    Console.WriteLine("Anda memilih buah Apel");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih buah Mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Anda memilih buah Pisang");
                    break;
                default:
                    Console.WriteLine("Anda memilih yang lain");
                    break;
            }
        }

        static void ternary()
        {
            Console.WriteLine("--Ternary--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Nilai x lebih besar dari nilai y" :
                x < y ? "Nilai x lebih kecil dari nilai y" : "Nilai x sama dengan nilai y";

            Console.WriteLine(hasilTernary);
        }

        static void ifNested()
        {
            Console.WriteLine("--If Nested--");
            Console.Write("Masukkan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if(nilai >= 70 && nilai <= maxNilai)
            {
                Console.WriteLine("Kamu berhasil!");
                if(nilai == maxNilai)
                {
                    Console.WriteLine("Kamu KEREN!!");
                }
            }
            else if(nilai >= 0 && nilai < 70)
            {
                Console.WriteLine("Kamu GAGAL!!");
            }
            else
            {
                Console.WriteLine("Masukkan angka yang benar");
            }
        }

        static void elseStatement()
        {
            Console.WriteLine("--If Else Statement--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            else if (y > x)
            {
                Console.WriteLine("Nilai y lebih besar dari nilai x");
            }
            else
            {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }
        }

        static void ifStatement()
        {
            Console.WriteLine("--If Statement--");
            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y : ");
            y = int.Parse(Console.ReadLine());

            if(x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            if(y > x)
            {
                Console.WriteLine("Nilai y lebih besar dari nilai x");
            }
            if(x == y)
            {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }
        }
    }
}
