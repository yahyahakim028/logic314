﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //HACKERRANK
            //SimpleArraySum();
            //StrongPassword();
            //CamelCase();
            //CaesarCipher();
            //BilanganPrima();

            Console.ReadKey();
        }

        static void BilanganPrima()
        {
            Console.WriteLine("--Bilangan Prima--");
            Console.Write("Masukkan jumlah bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;
            if (bilangan >= 2)
            {
                for (int i = 2; i <= bilangan; i++)
                {
                    for (int j = 2; j < i; j++)
                    {
                        if ((i % j) == 0)
                        {
                            prima = false;
                            break;
                        }
                    }
                    if (prima)
                        Console.WriteLine("Bilangan " + i + " adalah bilangan prima");
                    prima = true;
                }
            }
            else
            {
                Console.WriteLine("Tidak ada bilangan prima yang bisa dituliskan");
                Console.ReadLine();
            }
        }

        static void CaesarCipher()
        {
            string s = "middle-Outz";
            int k = 2;
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            string alphabetrotate = alphabet.Substring(k) + alphabet.Substring(0, k);
            //Console.Write(alphabetrotate);
            string hasil = "";
            char[] arrayAlphabet = alphabet.ToCharArray();
            char[] arrayAlphabetrotate = alphabetrotate.ToCharArray();
            char[] array = s.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (alphabet.Contains(char.ToLower(array[i])))
                {
                    if (char.IsUpper(array[i]))
                    {
                        hasil += arrayAlphabetrotate[Array.IndexOf(arrayAlphabet, char.ToLower(array[i]))].ToString().ToUpper();
                    }
                    else
                    {
                        hasil += arrayAlphabetrotate[Array.IndexOf(arrayAlphabet, char.ToLower(array[i]))].ToString().ToLower();
                    }
                }
                else
                {
                    hasil += array[i].ToString();
                }
            }

            Console.WriteLine(hasil);
        }

        static void CamelCase()
        {
            string s = "saveChangesInTheEditor";
            int hasil = 0;
            //hasil = Regex.Split(s, @"(?<!^)(?=[A-Z])").Length;
            char[] array = s.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0 || char.IsUpper(array[i]))
                {
                    hasil++;
                }
            }
            Console.WriteLine(hasil);
        }

        static void StrongPassword()
        {
            int n = 11;
            string password = "#HackerRank";
            int hasil = 0;
            string numbers = "0123456789";
            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string special_characters = "!@#$%^&*()-+";

            int no = 1, lower = 1, upper = 1, special = 1;

            char[] array = password.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (numbers.Contains(array[i]) && no == 1)
                {
                    no = 0;
                }
                else if (lower_case.Contains(array[i]) && lower == 1)
                {
                    lower = 0;
                }
                else if (upper_case.Contains(array[i]) && upper == 1)
                {
                    upper = 0;
                }
                else if (special_characters.Contains(array[i]) && special == 1)
                {
                    special = 0;
                }
            }

            hasil = Math.Max(6 - n, no + lower + upper + special);

            Console.WriteLine(hasil);
        }

        static void SimpleArraySum()
        {
            Console.WriteLine("--Simple Array Sum--");

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            //List<int> list2 = new List<int>() { 1,2,3,4,10,11 };

            int hasil = simpleArraySum(list);

            Console.WriteLine(hasil);
        }

        static int simpleArraySum(List<int> ar)
        {
            int result = 0;

            for (int i = 0; i < ar.Count; i++)
            {
                result += ar[i];
            }

            return result;
        }

    }
}
