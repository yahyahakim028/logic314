﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Soal1();
            //Soal2();
            Soal3();

            Console.ReadKey();
        }

        static void Soal3()
        {
            Console.WriteLine("--N=7 O=2 (StarCase Kelipatan 2 Segitiga Siku-siku, O = Kelipatan dan Start awal)--");
            Console.Write("Masukkan nilai (N) : ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai (O) : ");
            int o = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                int x = 0;
                for (int j = 0; j < n; j++)
                {
                    x += 2;
                    if (j == 0 || i == n - 1 || i == j)
                    {
                        Console.Write(x.ToString() + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }
        }

        static void Soal2()
        {
            Console.WriteLine("--Kue Pukis--");
            Console.Write("Masukkan Kue Pukis (N) : ");
            int n = int.Parse(Console.ReadLine());

            double teriguPerKue = 115d / 15d;
            double gulaPerKue = 190d / 15d;
            double susuPerKue = 100d / 15d;

            double terigu = 0, gula = 0, susu = 0;
            terigu = teriguPerKue * n;
            gula = gulaPerKue * n;
            susu = susuPerKue * n;

            Console.WriteLine($"{n} Kue Pukis membutuhkan Bahan : ");
            Console.WriteLine($"{terigu}gr terigu");
            Console.WriteLine($"{gula}gr gula pasir");
            Console.WriteLine($"{susu}mL susu");
        }

        static void Soal1()
        {
            Console.WriteLine("--Supir Ojol (Liter bensin dari Toko ke Customer)--");
            Console.Write("Masukkan Jumlah Customer : ");
            int customer = int.Parse(Console.ReadLine());

            double jarakCustomer1 = 2, jarakCustomer2 = 0.5, jarakCustomer3 = 1.5, jarakCustomer4 = 0.3, bensinPerLiter = 2.5;
            double totalBensin = 0, totalJarak = 0;
            if(customer == 1)
            {
                totalJarak = jarakCustomer1;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 2)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 3)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2 + jarakCustomer3;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM + {jarakCustomer3}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else if (customer == 4)
            {
                totalJarak = jarakCustomer1 + jarakCustomer2 + jarakCustomer3 + jarakCustomer4;
                totalBensin = totalJarak % bensinPerLiter == 0 ? (totalJarak / bensinPerLiter) : Math.Round(totalJarak / bensinPerLiter, MidpointRounding.ToPositiveInfinity);
                Console.WriteLine($"Jaram Tempuh : {jarakCustomer1}KM + {jarakCustomer2}KM + {jarakCustomer3}KM + {jarakCustomer4}KM = {totalJarak}KM");
                Console.WriteLine($"Bensin : {totalBensin} Liter");
            }
            else
            {
                Console.WriteLine($"Jumlah Customer tidak kurang dari 1 dan tidak lebih dari 4");
            }
        }

    }
}
