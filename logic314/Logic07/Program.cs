﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari.....");
        }
    }

    class Kucing : Mamalia
    {

    }

    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang.....");
        }
    }

    public class Program
    {
        static List<User> listUsers = new List<User>();
        static void Main(string[] args)
        {
            //bool ulangi = true;

            //while (ulangi)
            //{
            //    insertUser();
            //}

            //listUser();
            //dateTime();
            //stringDateTime();
            //timeSpan();
            //classInheritance();
            //overriding();

            Console.ReadKey();
        }

        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }

        static void classInheritance()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.Civic();
        }

        static void timeSpan()
        {
            DateTime date1 = new DateTime(2023, 1, 1, 1, 0, 0);
            DateTime date2 = DateTime.Now;

            //TimeSpan span = date1.Subtract(date2);
            TimeSpan span = date2 - date1;

            Console.WriteLine($"Total Hari : {span.Days}");
            Console.WriteLine($"Total Hari : {span.TotalDays}");
            Console.WriteLine($"Interval Jam : {span.Hours}");
            Console.WriteLine($"Total Jam : {span.TotalHours}");
            Console.WriteLine($"Total Menit : {span.TotalMinutes}");
            Console.WriteLine($"Total Detik : {span.TotalSeconds}");
        }

        static void stringDateTime()
        {
            Console.WriteLine("--String DateTime--");
            Console.Write("Masukkan tanggal (dd/mm/yyyy) : ");
            string strTanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strTanggal);

            Console.WriteLine($"Tgl : {tanggal.Day}");
            Console.WriteLine($"Bulan : {tanggal.Month}");
            Console.WriteLine($"Tahun : {tanggal.Year}");
            Console.WriteLine($"DayOfWeek ke : {(int)tanggal.DayOfWeek}");
            Console.WriteLine($"DayOfWeek ke : {tanggal.DayOfWeek}");
        }

        static void dateTime()
        {
            Console.WriteLine("--DateTime--");
            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);

            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 3, 9);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);
        }

        static void insertUser()
        {
            Console.WriteLine("--List User--");
            Console.Write("Masukkan Nama : ");
            string nama = Console.ReadLine();
            Console.Write("Masukkan Umur : ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Alamat : ");
            string alamat = Console.ReadLine();

            //List<User> listUsers = new List<User>();

            User user = new User();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listUsers.Add(user);

            for (int i = 0; i < listUsers.Count; i++)
            {
                Console.WriteLine($"Nama : {listUsers[i].Nama}\tUmur : {listUsers[i].Umur}\tAlamat : {listUsers[i].Alamat}");
            }
        }

        static void listUser()
        {
            Console.WriteLine("--List User--");
            List<User> listUser = new List<User>()
            {
                new User(){Nama = "Firdha", Umur = 24, Alamat = "Tangsel"},
                new User(){Nama = "Isni", Umur = 22, Alamat = "Cimahi"},
                new User(){Nama = "Asti", Umur = 23, Alamat = "Garut"},
                new User(){Nama = "Muafa", Umur = 22, Alamat = "Bogor"},
                new User(){Nama = "Toni", Umur = 24, Alamat = "Garut"}
            };

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"Nama : {listUser[i].Nama}\tUmur : {listUser[i].Umur}\tAlamat : {listUser[i].Alamat}");
            }
        }
    }
}
