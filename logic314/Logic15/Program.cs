﻿using System;
using System.Collections.Generic;

namespace Logic15
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //SIMULASI LOGIC
            //Soal1();
            //Soal2();
            //Soal6();
            //Soal9();
            //SoalKisiKisi_1();
            //SoalKisiKisi_2();
            //SoalKisiKisi_3();
            //SoalTesAI();
            SoalRekursifASC(); // fungsi yg memanggil dirinya sendiri
            SoalRekursifDESC(); // fungsi yg memanggil dirinya sendiri

            Console.ReadKey();
        }

        static void SoalRekursifDESC()
        {
            Console.WriteLine("--Soal Rekursif Function (DESC)--");
            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());

            //panggil fungsi
            PerulanganDESC(input);
        }

        static int PerulanganDESC(int input)
        {
            if (input == 1)
            {
                Console.WriteLine($"{input}");
                return input;
            }

            Console.WriteLine($"{input}");
            return PerulanganDESC(input - 1);
        }

        static void SoalRekursifASC()
        {
            Console.WriteLine("--Soal Rekursif Function (ASC)--");
            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());

            int start = 1;
            //panggil fungsi
            PerulanganASC(input, start);
        }

        static int PerulanganASC(int input, int start)
        {
            if(start == input)
            {
                Console.WriteLine($"{start}");
                return start;
            }

            Console.WriteLine($"{start}");
            return PerulanganASC(input, start + 1);
        }

        static void Soal6()
        {
            Console.WriteLine("--Soal 6--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";

            bool pangram = true;
            foreach(char item in alfabet)
            {
                if (!kalimat.Contains(item))
                {
                    pangram = false;
                    break;
                }
            }
            if (pangram)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }
        }

        static void SoalTesAI()
        {
            //SinglyLinkedListNode
            //SinglyLinkedList.head
            //SinglyLinkedListNode
            //LinkedList<string> list = new LinkedList<string>(); 
            //LinkedListNode.

            //LinkedListNode tes = new LinkedListNode();
            //tes.
            //SinglyLinkedList

        }

        static void SoalKisiKisi_3()
        {
            //List<int> list = new List<int>(){ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);
            
            //int a = 
            
                for (int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                    list.RemoveAt(i);
            }

            list.Insert(1, 20);
            Console.WriteLine($"9 index ke : {list.IndexOf(9)}");

            Console.WriteLine(String.Join(", ", list));
        }

        static void SoalKisiKisi_2()
        {
            Console.WriteLine("--Soal Kisi-Kisi 2--");
            Console.Write("Masukkan bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;
            int tampung = 0;
            if (bilangan >= 2)
            {
                //for(int i = 2; i <= bilangan; i++) //UNTUK PRINT ALL PRIMA
                //{
                for (int j = 2; j < bilangan; j++) //GANTI bilangan jadi i
                {
                    if (bilangan % j == 0) //GANTI bilangan jadi i
                    {
                        tampung = j;
                        prima = false;
                        //break; //UNTUK PRINT PEMBAGI TERKECIL
                    }
                }
                if (prima)
                {
                    Console.WriteLine($"1");
                }
                else
                {
                    Console.WriteLine($"{tampung}");
                }
                prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan bukan bilangan prima");
            }
        }

        static void SoalKisiKisi_1()
        {
            //List<int> list = new List<int>(){ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                    list.RemoveAt(i);
            }

            Console.WriteLine(String.Join(", ", list));
        }

        static void Soal9()
        {
            Console.WriteLine("--Soal 9--");
            Console.Write("Masukkan beli pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());

            int point1 = 0, point2 = 0;
            if(pulsa <= 10000)
            {
                Console.WriteLine($"0 = 0 Point");
            }
            else if(pulsa <= 30000)
            {
                point1 = (pulsa - 10000) / 1000; 
                Console.WriteLine($"0 + {point1} = {point1} Point");
            }
            else
            {
                point1 = (30000 - 10000) / 1000;
                point2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine($"0 + {point1} + {point2} = {point1 + point2} Point");
            }

        }

        static void Soal2()
        {
            Console.WriteLine("--Soal 2--");
            Console.Write("Masukkan start : ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("Masukkan end : ");
            int end = int.Parse(Console.ReadLine());

            string initial = "XA-";

            DateTime date = DateTime.Now;

            for(int i = start; i <= end; i++)
            {
                Console.WriteLine($"{initial}{date.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5, '0')}");
            }

        }

        static void Soal1()
        {
            Console.WriteLine("--Soal 1--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            //char[] array = kalimat.ToCharArray();
            int count = 0, index = 0;
            foreach (char item in kalimat.ToCharArray())
            {
                if (char.IsUpper(item) || index == 0)
                {
                    count++;
                }
                index++;
            }

            Console.WriteLine(count);
        }

    }
}
