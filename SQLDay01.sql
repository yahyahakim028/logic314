--DDL

--create database
create database db_kampus

use db_kampus

--create table 
create table mahasiswa (
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

create table biodata(
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[dob] [date] NOT NULL,
	[kota] [varchar](100) NULL,
	[mahasiswa_id] [bigint] NOT NULL
)

--create view
create view vwMahasiswa
as 
select id,name,address,email from mahasiswa

--select view
select * from vwMahasiswa

-----------------------

--alter add column
alter table mahasiswa add [description] varchar(255)

--alter drop column
alter table mahasiswa drop column [description]

--table alter column
alter table mahasiswa alter column email varchar(100)

-----------------------

--drop database
drop database db_kampus2

--drop table
drop table mahasiswa2

--drop view
drop view vwMahasiswa

------------------------

--DML

--insert data
insert into mahasiswa(name,address,email) 
values
('Marchel','Medan','marchelajadeh@gmail.com'),
('Toni','Garut','toni@gmail.com'),
('Isni','Cimahi','isni@gmail.com')

insert into biodata(dob,kota,mahasiswa_id) 
values
('2000-03-15','Daan Mogot',1),
('1998-08-05','Kalibata',4),
('2000-06-26','Gandaria',3)

--select data
select id,name,address,email from mahasiswa
select * from mahasiswa

--update data
update mahasiswa set name='Isni Dwitiniardi',address='Sumedang' where name='isni'

--delete data
delete from mahasiswa where name='toni'

--join table
select mhs.id as ID,mhs.name,bio.kota,bio.dob,
month(bio.dob)BulanLahir,year(bio.dob)TahunLahir
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.name = 'toni' or bio.kota='jakarta' --AND OR

--order by
select * 
from mahasiswa mhs
join biodata bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc,mhs.name desc

--select top
select top 2 * from mahasiswa order by name desc

--between
select * from mahasiswa where id between 1 and 3
select * from mahasiswa where id >= 1 and id <= 3

--like
select * From mahasiswa where name like 'm%'
select * From mahasiswa where name like '%i'
select * From mahasiswa where name like '%dwi%'
select * from mahasiswa where name like '_o%'
select * From mahasiswa where name like '__n%'
select * From mahasiswa where name like 't_%'
select * From mahasiswa where name like 't____%'
select * From mahasiswa where name like 'i%i'

--group by
select count(id),name From mahasiswa group by name

--having
select count(id),name 
from mahasiswa 
group by name
having count(id) > 1

--distinct
select distinct name From mahasiswa

--SubString
select SUBSTRING('SQL Tutorial', 1, 3) as Judul

--CharIndex
select CHARINDEX('t', 'Customer') as Judul

--DataLength
select DATALENGTH('akumau.istirahat')

--case when
select id,name,address,panjang,
case when panjang < 50 then 'Pendek'
when panjang <= 100 then 'Sedang'
else 'Tinggi'
end as TinggiBadan
from mahasiswa

--concat
select CONCAT('SQL ', ' is ', ' fun!')
select 'SQL ' + ' is ' + ' fun!'

--Operator Aritmatika
create table penjualan(
id bigint primary key identity(1,1),
nama varchar(50) not null,
harga int not null
)
insert into penjualan(nama,harga)
values
('Indomie',1500),
('Close-Up',3500),
('Pepsodent',3000),
('Brush Formula',2500),
('Roti Manis',1000),
('Gula',3500),
('Sarden',4500),
('Rokok Sampurna',11000),
('Rokok 234',11000)
select *,harga * 100 as [Harga * 100] from penjualan

sp_columns 'mahasiswa'
sp_help 'mahasiswa'
